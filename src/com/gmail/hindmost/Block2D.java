package com.gmail.hindmost;

/**
 * Created by kir on 4/7/17.
 */
public class Block2D implements IBlock {

    // "number" of the tree
    int mark;

    // kind of block (chosen from the set the algorithm cares about)
    BlockKind blockKind = BlockKind.Air;

    // stubbed reaction on `notifyBlock`
    boolean standing = true;

    public Block2D(int mark, BlockKind blockKind) {
        this.mark = mark;
        this.blockKind = blockKind;
    }

    // the type is BlockKind.Air by default
    public Block2D(int mark) {
        this.mark = mark;
    }

    @Override
    public int getMark() {
        return mark;
    }

    @Override
    public BlockKind getBlockKind() {
        return blockKind;
    }

    @Override
    public void notifyBlock() {
        standing = false;
    }

    // to pretty-print in console
    @Override
    public String toString() {
        String open = "?", close = "?";
        switch (blockKind) {
            case Leaf:   open = "("; close = ")"; break;
            case Thunk:  open = "["; close = "]"; break;
            case Air:    open = " "; close = " "; break;
            case Gravel: open = "#"; close = "#"; break;
        }
        return open + (standing? "" + mark : "X") + close;
    }
}
