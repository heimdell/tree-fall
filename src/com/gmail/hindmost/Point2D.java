package com.gmail.hindmost;

import java.util.HashSet;
import java.util.Vector;

/**
 * Created by kir on 4/7/17.
 */
public class Point2D implements IPoint {
    int x, y;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    private int lin_distance(Point2D pt) {
        return Math.abs(x - pt.x) + Math.abs(y - pt.y);
    }

    @Override
    public Vector<IPoint> neighbors(int lin_dist) {
        Vector<IPoint> near = new Vector<>();
        for (int dx = -lin_dist; dx <= lin_dist; dx++) {
            for (int dy = -lin_dist; dy <= lin_dist; dy++) {
                Point2D pt = new Point2D(x + dx, y + dy);
                if (this.lin_distance(pt) <= lin_dist) {
                    near.add(pt);
                }
            }
        }
        return near;
    }

    @Override
    public boolean notBelow(IPoint pt) {
        return pt.getHeight() <= this.getHeight();
    }

    @Override
    public int getHeight() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    @Override
    public int hashCode() {
        int result = 17;
        result *= 31; result += x;
        result *= 31; result += y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Point2D && obj.hashCode() == this.hashCode();
    }
}
