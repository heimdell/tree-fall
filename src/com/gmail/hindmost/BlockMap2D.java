package com.gmail.hindmost;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kir on 4/7/17.
 */
public class BlockMap2D implements IBlockMap {
    HashMap<IPoint, IBlock> map = new HashMap<>();

    @Override
    public IBlock at(IPoint pt) {
        IBlock block = map.get(pt);
        return block == null? new Block2D(0, BlockKind.Air) : block;
    }

    public void add(IPoint pt, IBlock block) {
        map.put(pt, block);
    }

    @Override
    public String toString() {
        Point2D min = new Point2D(1000, 1000),
                max = new Point2D(-1000, -1000);
        for (IPoint pt : map.keySet()) {
            Point2D pt1 = (Point2D) pt;
            min = new Point2D(Math.min(min.x, pt1.x), Math.min(min.y, pt1.y));
            max = new Point2D(Math.max(max.x, pt1.x), Math.max(max.y, pt1.y));
        }
        String res = "";
        for (int iy = max.y; iy >= min.y; iy--) {
            for (int ix = min.x; ix <= max.x; ix++) {
                Point2D pt = new Point2D(ix, iy);
                IBlock b = at(pt);
                res += b + " ";
            }
            res += "\n";
        }
        return res;
    }
}
