package com.gmail.hindmost;

/**
 * Created by kir on 4/7/17.
 */
public enum BlockKind {
    Leaf,
    Thunk,
    Air,    // anything neither leafy nor thunk-like nor able to fall
    Gravel  // anything able to fall
}
