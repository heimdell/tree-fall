package com.gmail.hindmost;

import java.util.Vector;

/**
 * Created by kir on 4/7/17.
 */
public interface IPoint {
    Vector<IPoint> neighbors(int linear_distance);
    boolean notBelow(IPoint pt);
    int getHeight();
}
