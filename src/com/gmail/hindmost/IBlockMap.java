package com.gmail.hindmost;

/**
 * Created by kir on 4/7/17.
 */
public interface IBlockMap {
    IBlock at(IPoint pt);
}
