package com.gmail.hindmost;

/**
 * Created by kir on 4/7/17.
 */
public interface IBlock {
    int getMark();
    BlockKind getBlockKind();
    void notifyBlock();
}
