package com.gmail.hindmost;

public class Main {

    public static void main(String[] args) {
        Point2D pt = new Point2D(0,0);

        Block2D thunkStart  = new Block2D(1, BlockKind.Thunk);
        Block2D thunkNext   = new Block2D(1, BlockKind.Thunk);
        Block2D leafFirst   = new Block2D(1, BlockKind.Leaf);
        Block2D leafSecond  = new Block2D(1, BlockKind.Leaf);
        Block2D thunkOnLeaf = new Block2D(1, BlockKind.Thunk);
        Block2D alienThunk  = new Block2D(2, BlockKind.Thunk);
        Block2D thunkBelow  = new Block2D(1, BlockKind.Thunk);
        Block2D boring      = new Block2D(0, BlockKind.Air);
        Block2D gravel      = new Block2D(0, BlockKind.Gravel);
        Block2D sand        = new Block2D(0, BlockKind.Gravel);
        Block2D leafOnSand  = new Block2D(1, BlockKind.Leaf);

        BlockMap2D map = new BlockMap2D();
        map.add(new Point2D(1, -1), thunkBelow);
        map.add(new Point2D(1, 0), thunkStart);
        map.add(new Point2D(0, 1), alienThunk);
        map.add(new Point2D(1, 1), thunkNext);
        map.add(new Point2D(2, 1), boring);
        map.add(new Point2D(1, 2), leafFirst);
        map.add(new Point2D(2, 2), leafSecond);
        map.add(new Point2D(2, 3), thunkOnLeaf);
        map.add(new Point2D(0, 1), alienThunk);
        map.add(new Point2D(1, 3), gravel);
        map.add(new Point2D(0, 2), sand);
        map.add(new Point2D(0, 3), leafOnSand);

        IBlock b = map.at(new Point2D(1, 0));

        Worker worker = new Worker(map, 1);

        worker.seed(new Point2D(1, 0));

        try {
            while (worker.step())
                System.out.println(map.toString());
        } catch (Exception e) {

        }
    }
}
