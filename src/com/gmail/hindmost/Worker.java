package com.gmail.hindmost;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;

/**
 * Created by kir on 4/7/17.
 */
public class Worker {
    HashSet<IPoint> visited    = new HashSet<>();
    Queue<Task>     inProgress = new ArrayDeque<>();
    IBlockMap       map;
    int             distance;
    int             mark;
    IPoint          initial;

    public Worker(IBlockMap map, int distance) {
        this.map      = map;
        this.distance = distance;
    }

    public void seed(IPoint initial) {
        inProgress.add(new Task(initial, BlockKind.Thunk));
        this.mark = map.at(initial).getMark();
        this.initial = initial;
    }

    public boolean step() throws Exception {
        if (inProgress.isEmpty())
            return false;

        Task      task   = inProgress.poll();
        IPoint    point  = task.getPoint();
        BlockKind source = task.getBlockKind();
        IBlock    block  = map.at(point);
        BlockKind kind   = block.getBlockKind();

        switch (kind) {
            case Leaf:
                if (block.getMark() == mark) {
                    block.notifyBlock();
                    enqueueNeighbors(point, kind);
                }
                break;

            case Thunk:
                if (block.getMark() == mark && source == BlockKind.Thunk) {
                    block.notifyBlock();
                    enqueueNeighbors(point, kind);
                }
                break;

            case Gravel:
                block.notifyBlock();
                break;

            default:
                break;
        }
        return true;
    }

    private void enqueueNeighbors(IPoint point, BlockKind kind) {
        for (IPoint near : point.neighbors(distance)) {
            boolean wellPlaced = near.notBelow(initial);
            if (!visited.contains(near) && wellPlaced) {
                visited.add(near);
                inProgress.add(new Task(near, kind));
            }
        }
    }

    @Override
    public String toString() {
        String res = "";
        for (IPoint touched : visited) {
            res += touched + " :: ";
        }
        res += "[]; {";
        for (Task task : inProgress) {
            res += task.getBlockKind() + ": " + task.getPoint() + ", ";
        }
        res += "}";
        return res;
    }

    class Task {
        IPoint    pt;
        BlockKind source;

        public Task(IPoint pt, BlockKind source) {
            this.pt     = pt;
            this.source = source;
        }

        public IPoint getPoint() {
            return pt;
        }

        public BlockKind getBlockKind() {
            return source;
        }
    }
}
